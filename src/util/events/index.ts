import { READY } from "./READY";
import { CHANNEL_CREATE } from "./CHANNEL_CREATE";
import { GUILD_DELETE } from "./GUILD_DELETE";
import { GUILD_CREATE } from "./GUILD_CREATE";
import { CHANNEL_DELETE } from "./CHANNEL_DELETE";
import { MESSAGE_CREATE } from "./MESSAGE_CREATE";

const Events = {
	READY: READY,
	CHANNEL_CREATE: CHANNEL_CREATE,
	GUILD_DELETE: GUILD_DELETE,
	GUILD_CREATE: GUILD_CREATE,
	CHANNEL_DELETE: CHANNEL_DELETE,
	MESSAGE_CREATE: MESSAGE_CREATE,
};

export default Events;
