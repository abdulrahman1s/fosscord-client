export interface Channel {
	id: string;
	name: string;
	parent_id: string;
	type: number;
	collapsed: boolean;
}
